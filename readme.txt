Itunes Music Player

Feature
1. Search song or movie preview based on keyword
2. Select song or movie to play
3. Play and stop button
4. Go to next or previous track

Device testing
Xiaomi Redmi 5a with Android 10 OS.

[-] App

    [-] Adapter
        // Collection of binding, included binding adapter and recyclerview adapter

    [-] Base
        // Contain Application and base for activity and fragment

    [-] Main
        // Starter Activity are here (MainActivity)

        [-] ViewModel
            // MainActivity's view model

    [-] Model
        // Collection of all model response from server.

    [-] Network
        [-] Module
            // Collection of all modules for Dependency injection purpose.

        [-] Repository
            // Repository collection

        [-] Service
            // All service api listed here



-----------------  Github for this project  -----------------
https://gitlab.com/latif.albar13/itunesmusicplayer
branch : [MASTER]


