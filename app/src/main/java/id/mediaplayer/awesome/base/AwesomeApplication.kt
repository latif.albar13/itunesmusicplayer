package id.mediaplayer.awesome.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AwesomeApplication : Application()