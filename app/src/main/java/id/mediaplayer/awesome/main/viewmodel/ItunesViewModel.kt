package id.mediaplayer.awesome.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.mediaplayer.awesome.model.ItunesData
import id.mediaplayer.awesome.network.repository.ItunesRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class ItunesViewModel @Inject constructor(var repository: ItunesRepository) : ViewModel() {

    val mListMusic = mutableListOf<ItunesData>()
    val isMusicFinishLoaded = MutableLiveData<Boolean>().apply { value = false }
    var isLoading = MutableLiveData<Boolean>().apply { value = false }
    var play = MutableLiveData<Boolean>().apply { value = false }
    var playNext = MutableLiveData<Boolean>().apply { value = false }
    var playPrev = MutableLiveData<Boolean>().apply { value = false }

    init {
        getItunesList()
    }

    private fun getItunesList() {
        isLoading.postValue(true)
        repository.getItunesList("test")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                isLoading.postValue(false)
                mListMusic.addAll(it.itunesData)
                isMusicFinishLoaded.postValue(true)
            }
            .subscribe()
    }

    fun onTextChanged(text: CharSequence) {
        Observable.just(text)
            .observeOn(Schedulers.io())
            .doOnNext { isLoading.postValue(true) }
            .flatMap { repository.getItunesList(text.toString()) }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                isLoading.postValue(false)
                mListMusic.clear()
                mListMusic.addAll(it.itunesData)
                isMusicFinishLoaded.postValue(true)
            }
            .subscribe()
    }

    fun playMusic() {
        if (play.value == false) {
            play.postValue(true)
        } else {
            play.postValue(false)
        }
    }

    fun playNextTrack() {
        playNext.postValue(true)
    }

    fun playPrevTrack() {
        playPrev.postValue(true)
    }
}