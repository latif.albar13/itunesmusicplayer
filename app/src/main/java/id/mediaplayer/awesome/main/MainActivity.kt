package id.mediaplayer.awesome.main

import android.media.MediaPlayer
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import id.mediaplayer.awesome.R
import id.mediaplayer.awesome.adapter.MusicAdapter
import id.mediaplayer.awesome.base.BaseActivity
import id.mediaplayer.awesome.databinding.ActivityMainBinding
import id.mediaplayer.awesome.main.viewmodel.ItunesViewModel
import id.mediaplayer.awesome.model.ItunesData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    companion object {
        const val DELAY_TIME = 1000L
    }

    private lateinit var binding: ActivityMainBinding
    private val vm: ItunesViewModel by viewModels()
    private var player: MediaPlayer = MediaPlayer()
    private var mPrevSelectedPosition = 0
    private var mBottomSheetBehavior: BottomSheetBehavior<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initRecyclerLinearLayout()
        initMusicClickListener()
        initBottomSheet()
        initSearch()
        initBinding()
        initActionListener()
    }

    private fun initBinding() {
        with(binding) {
            lifecycleOwner = this@MainActivity
            viewModel = vm
        }
    }

    private fun initActionListener() {
        with(vm) {
            isMusicFinishLoaded.observe(this@MainActivity, Observer {
                if (it) {
                    getItunesAdapter().addAll(vm.mListMusic)
                }
            })
            isLoading.observe(this@MainActivity, Observer {
                if (it && player.isPlaying.not()) {
                    mBottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
                }
            })
            play.observe(this@MainActivity, Observer {
                if (it) {
                    play()
                } else {
                    stop()
                }
            })

            playPrev.observe(this@MainActivity, Observer {
                if (it) {
                    if (mPrevSelectedPosition - 1 == -1) {
                        launchMusicPlayer(vm.mListMusic[0])
                    } else {
                        val prevPosition = mPrevSelectedPosition - 1 //Get prev position
                        resetVisualizer(vm.mListMusic[prevPosition], prevPosition)
                        launchMusicPlayer(vm.mListMusic[prevPosition])
                    }
                }
            })

            playNext.observe(this@MainActivity, Observer {
                if (it) {
                    if (mPrevSelectedPosition > vm.mListMusic.size - 1) {
                        launchMusicPlayer(vm.mListMusic[0])
                    } else {
                        val nextPosition = mPrevSelectedPosition + 1 //Get next position
                        resetVisualizer(vm.mListMusic[nextPosition], nextPosition)
                        launchMusicPlayer(vm.mListMusic[nextPosition])
                    }
                }
            })
        }
    }

    private fun initRecyclerLinearLayout() {
        binding.rvMusic.layoutManager = LinearLayoutManager(this)
    }

    private fun initMusicClickListener() {
        getItunesAdapter().setItemMusicCallback(object : MusicAdapter.ItemMusicClickListener {
            override fun onItemMusicClick(position: Int, music: ItunesData) {
                if (music.previewUrl.isNotEmpty()) {
                    mBottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
                    resetVisualizer(music, position)
                    launchMusicPlayer(music)
                } else {
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.error_playback),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun resetVisualizer(music: ItunesData, position: Int) {
        vm.mListMusic[mPrevSelectedPosition].selected = false //remove previous pick pos
        music.selected = true //mark current position
        vm.mListMusic[position] = music //update music with new selected value
        getItunesAdapter().addAll(vm.mListMusic)
        mPrevSelectedPosition = position //mark previous position with new value.
    }

    private fun launchMusicPlayer(music: ItunesData) {
        player.reset()
        player.setDataSource(music.previewUrl)
        player.prepare()
        vm.play.postValue(true)
    }

    private fun getItunesAdapter(): MusicAdapter {
        val adapter: MusicAdapter
        if (binding.rvMusic.adapter != null) {
            adapter = binding.rvMusic.adapter as MusicAdapter
        } else {
            adapter = MusicAdapter()
            binding.rvMusic.adapter = adapter
        }
        return adapter
    }

    private fun initBottomSheet() {
        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        mBottomSheetBehavior?.peekHeight = 0
        mBottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun textChangeListenerObservable(): Observable<String> {
        return Observable.create<String> {
            binding.etSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    it.onNext(s.toString())
                }

                override fun afterTextChanged(s: Editable?) {
                }
            })
        }.debounce(DELAY_TIME, TimeUnit.MILLISECONDS)
    }

    private fun initSearch() {
        Observable.just(true)
            .observeOn(AndroidSchedulers.mainThread())
            .observeOn(Schedulers.io())
            .flatMap { textChangeListenerObservable() }
            .map { vm.onTextChanged(it) }
            .subscribe()
    }

    private fun play() {
        if (player.isPlaying) {
            player.stop()
        }

        player.start()
    }

    private fun stop() {
        player.pause()
    }
}

