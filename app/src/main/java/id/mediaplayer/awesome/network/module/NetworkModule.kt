package id.mediaplayer.awesome.network.module

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.mediaplayer.awesome.network.service.ItunesService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.InetAddress
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
open class NetworkModule {
    @Singleton
    @Provides
    fun provideItunesService(@Named("service_retrofit") retrofit: Retrofit): ItunesService {
        return retrofit.create(ItunesService::class.java)
    }

    @Singleton
    @Provides
    @Named("service_retrofit")
    fun provideRetrofit(
        @Named("ItunesAppOkHttpClient") okHttpClient: OkHttpClient,
        @Named("gson_converter") gsonConverter: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://itunes.apple.com/")
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(gsonConverter)
            .build()
    }

    @Singleton
    @Provides
    @Named("gson_converter")
    fun provideConverterFactory(@Named("lower_case_with_underscores_gson") gson: Gson):
            Converter.Factory {
        return GsonConverterFactory.create(gson)
    }

    @Singleton
    @Provides
    @Named("ItunesAppOkHttpClient")
    fun provideOkHttpClient(@ApplicationContext context: Context): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(buildHttpLoggingInterceptor())
            .addInterceptor(buildHeaderInterceptor())
            .connectTimeout(4, TimeUnit.MINUTES)
            .readTimeout(4, TimeUnit.MINUTES)
            .writeTimeout(4, TimeUnit.MINUTES)
            .build()
    }

    private fun buildHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    private fun buildHeaderInterceptor(): Interceptor {
        return Interceptor { chain ->
            val request = chain.request()
            val requestBuilder = request
                .newBuilder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")

            if (!isConnected()) {
                throw IOException("No Connection")
            } else {
                chain.proceed(requestBuilder.build())
            }
        }
    }

    private fun isConnected(): Boolean {
        return try {
            val ipAddr = InetAddress.getByName("google.com")
            !ipAddr.equals("")

        } catch (e: Exception) {
            false
        }
    }
}