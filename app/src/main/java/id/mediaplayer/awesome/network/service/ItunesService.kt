package id.mediaplayer.awesome.network.service

import id.mediaplayer.awesome.model.ItunesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ItunesService {

    @GET("search")
    fun getItunesList(@Query("term") term: String): Observable<ItunesResponse>

}


