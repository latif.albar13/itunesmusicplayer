package id.mediaplayer.awesome.network.module

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.mediaplayer.awesome.network.repository.ItunesRepository
import id.mediaplayer.awesome.network.service.ItunesService
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ItunesRepositoryModule {

    @Provides
    @Singleton
    fun provideItunesRepository(itunesService: ItunesService): ItunesRepository {
        return ItunesRepository(itunesService)
    }
}