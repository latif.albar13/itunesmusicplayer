package id.mediaplayer.awesome.network.repository

import id.mediaplayer.awesome.model.ItunesResponse
import id.mediaplayer.awesome.network.service.ItunesService
import io.reactivex.Observable

class ItunesRepository(private var itunesService: ItunesService) {
    fun getItunesList(term: String): Observable<ItunesResponse> {
        return itunesService.getItunesList(term)
            .onErrorResumeNext { it: Throwable ->
                Observable.just(ItunesResponse())
            }
    }
}