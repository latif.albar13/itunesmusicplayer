package id.mediaplayer.awesome.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.mediaplayer.awesome.databinding.ItemMusicBinding
import id.mediaplayer.awesome.model.ItunesData

class MusicAdapter : RecyclerView.Adapter<MusicAdapter.ViewHolder>() {

    private var mList = mutableListOf<ItunesData>()
    private lateinit var mListener: ItemMusicClickListener

    fun addAll(list: MutableList<ItunesData>) {
        mList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemMusicBinding.inflate(inflater))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(mList[position], position)

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ViewHolder(val itemBinding: ItemMusicBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(music: ItunesData, position: Int) {
            itemBinding.music = music
            itemBinding.executePendingBindings()
            itemBinding.root.setOnClickListener {
                mListener.onItemMusicClick(position, music)
            }
        }
    }

    interface ItemMusicClickListener {
        fun onItemMusicClick(position: Int, music: ItunesData)
    }

    fun setItemMusicCallback(listener: ItemMusicClickListener) {
        this.mListener = listener
    }

}