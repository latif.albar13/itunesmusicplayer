package id.mediaplayer.awesome.model


import com.google.gson.annotations.SerializedName

data class ItunesResponse(
    @SerializedName("resultCount")
    var resultCount: Int = 0,
    @SerializedName("results")
    var itunesData: List<ItunesData> = listOf()
)