package id.mediaplayer.awesome.main.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import id.mediaplayer.awesome.TestCoroutineRule
import id.mediaplayer.awesome.model.ItunesResponse
import id.mediaplayer.awesome.network.repository.ItunesRepository
import id.mediaplayer.awesome.network.service.ItunesService
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(
    fullyQualifiedNames = [
        "id.mediaplayer.awesome.*"]
)
class ItunesViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Rule
    @JvmField
    val test = TestCoroutineRule()

    private lateinit var itunesViewModel: ItunesViewModel

    @Mock
    lateinit var isLoading1: Observer<Boolean>


    lateinit var itunesRepository: ItunesRepository

    lateinit var itunesService: ItunesService

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)

        itunesService = PowerMockito.mock(ItunesService::class.java)
        val newObs = Observable.just(ItunesResponse(10)) // expectation count result
        PowerMockito.`when`(itunesService.getItunesList("test")).thenReturn(newObs)

        itunesRepository = ItunesRepository(itunesService)
        itunesViewModel = ItunesViewModel(itunesRepository)
    }

    /*
    * Test how many track count from api.
    * */
    @Test
    fun `get itunes count from service`() {
        var count = 0 // actual count result
        itunesViewModel
            .repository
            .getItunesList("test")
            .doOnNext { count = it.resultCount }
            .subscribe()
        Assert.assertEquals(10, count)
    }
}